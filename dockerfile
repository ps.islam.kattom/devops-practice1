FROM openjdk:11-jre-slim
COPY ./target/*.jar ./app.jar
EXPOSE 8090
ENTRYPOINT ["java","-jar","-Dserver.port=8090 ", "-Dspring.profiles.active=h2","app.jar"]